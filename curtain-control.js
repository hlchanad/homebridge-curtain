
var Promise = require('promise');

var Curtain = require('./lib/curtain');

module.exports = function () {

    var curtain = new Curtain();

    return {
        getIsOn: function () {
            return new Promise(function (resolve, reject) {
                resolve(curtain.isOpened());
            });
        },

        open: function () {
            return new Promise(function (resolve, reject) {
                curtain.goToTop() ? resolve() : reject('open curtain fail');
            });
        },

        close: function () {
            return new Promise(function (resolve, reject) {
                curtain.goToBottom() ? resolve() : reject('open curtain fail');
            });
        }
    }
}

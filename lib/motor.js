/**
 * Created by chanhonlun on 17/11/2017.
 */

var gpio = require('rpi-gpio');
var Promise = require('promise');

// DEMO
// gpio.setup(22, gpio.DIR_LOW, function () {
//     console.log('setup cb');
//
//     setTimeout(function() {
//         gpio.write(22, gpio.DIR_HIGH, function (err, val) {
//             console.log('write cb');
//
//             console.log('err', err);
//             console.log('val', val);
//         });
//     }, 1000);
// });

module.exports = function (inputA, inputB, enable) {

    this.inputA = inputA;
    this.inputB = inputB;
    this.enable = enable;

    this.initPins = () => {
        return new Promise((resolve, reject) => {
            var pins = [this.inputA, this.inputB, this.enable];

            var promises = pins.map((pin) => {
                return new Promise((resolve, reject) => {
                    gpio.setup(pin, gpio.DIR_LOW, err => err ? reject(err) : resolve());
                });
            });

            Promise.all(promises)
                .then(() => resolve())
                .catch(err => reject(err));
        });
    };

    this.goForward = (duration = null) => {
        return new Promise((resolve, reject) => {
            gpio.write(this.inputA, true, handleError);
            gpio.write(this.inputB, false, handleError);
            gpio.write(this.enable, true, handleError);

            if (duration) {
                setTimeout(() => {
                    this.stop();
                    resolve();
                }, duration);
            }
            else {
                resolve();
            }
        });
    };

    this.goBackward = (duration = null) => {
        return new Promise((resolve, reject) => {
            gpio.write(this.inputA, false, handleError);
            gpio.write(this.inputB, true, handleError);
            gpio.write(this.enable, true, handleError);

            if (duration) {
                setTimeout(() => {
                    this.stop();
                    resolve();
                }, duration);
            }
            else {
                resolve();
            }
        });
    };

    this.stop = () => {
        gpio.write(this.inputA, false, handleError);
        gpio.write(this.inputB, false, handleError);
        gpio.write(this.enable, false, handleError);
    };
};

const handleError = (err, value) => {
    // console.log('[handleError] err', err);
    // console.log('[handleError] value', value);
}

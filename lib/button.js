/**
 * Created by chanhonlun on 17/11/2017.
 */

var gpio = require('rpi-gpio');

 module.exports = function (pin) {

     gpio.setup(pin, gpio.DIR_IN, gpio.EDGE_RISING);

     // cb: (channel, value) => { ... }
     this.listen = (cb) => {
         gpio.on('change', cb);
     }
 }

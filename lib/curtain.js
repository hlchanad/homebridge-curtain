/**
 * Created by chanhonlun on 17/11/2017.
 */

var Motor = require('./motor');
var Button = require('./button');

module.exports = function () {

    this.MOTOR_INPUT_A = 16;
    this.MOTOR_INPUT_B = 18;
    this.MOTOR_ENABLE  = 22;
    this.BUTTON_UP     = 11;
    this.BUTTON_DOWN   = 13;

    this.UP_DURATION   = 2000;
    this.DOWN_DURATION = 2000;

    this.STATUS_OPENING = 'opening';
    this.STATUS_OPENED  = 'opened';
    this.STATUS_CLOSING = 'closing';
    this.STATUS_CLOSED  = 'closed';

    this.state = this.STATUS_CLOSED;

    this.position = 0; // 0: top, 100: bottom // TODO

    this.motor = new Motor(this.MOTOR_INPUT_A, this.MOTOR_INPUT_B, this.MOTOR_ENABLE);
    this.motor.initPins()
        .then(() => console.log('init-ed all motor control pins'))
        .catch(err => console.log('cannot init pins for motor'));

    this.buttonUp = new Button(this.BUTTON_UP);
    this.buttonUp.listen((channel, value) => value && this.goToTop());

    this.buttonDown = new Button(this.BUTTON_DOWN);
    this.buttonDown.listen((channel, value) => value && this.goToBottom());

    this.isOpened = () => {
        return [this.STATUS_OPENING, this.STATUS_OPENED].indexOf(this.state) >= 0;
    };

    this.goToTop = () => {

        if (isProcessing()) { return false; }

        this.state = this.STATUS_OPENING;
        this.motor.goForward(this.UP_DURATION)
            .then(() => {
                this.state = this.STATUS_OPENED;
                this.position = 0;
            });

        return true;
    };

    this.goToBottom = () => {

        if (isProcessing()) { return false; }

        this.state = this.STATUS_CLOSING;
        this.motor.goBackward(this.DOWN_DURATION)
            .then(() => {
                this.state = this.STATUS_CLOSED;
                this.position = 100;
            });

        return true;
    };

    const isProcessing = () => {
        return this.state === this.STATUS_OPENING || this.state === this.STATUS_CLOSING;
    };
};

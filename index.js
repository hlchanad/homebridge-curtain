
var PACKAGE_NAME     = "homebridge-curtain",
    ACCESSORY_NAME   = "MyCurtainSwitch",
    MANUFACTURER     = "chanhonlun"
    MODEL            = "CurtainCtrl-01",
    SERIAL_NUMBER    = "CC-01-001";

var Service, Characteristic;

module.exports = function (homebridge) {
    Service = homebridge.hap.Service;
    Characteristic = homebridge.hap.Characteristic;
    homebridge.registerAccessory(PACKAGE_NAME, ACCESSORY_NAME, CurtainSwitch);
};


/*
 * CurtainSwitch constructor, who will be instantiated by homebridge
 */
function CurtainSwitch (log, config) {
    this.log    = log;
    this.config = config;
    this.curtainControl = require('./curtain-control.js')();
}

/*
 * initialization for homebridge accessory
 * hook up all event and corresponding handler
 */
CurtainSwitch.prototype.getServices = function () {
    this.informationService = new Service.AccessoryInformation();
    this.informationService
        .setCharacteristic(Characteristic.Manufacturer, MANUFACTURER)
        .setCharacteristic(Characteristic.Model, MODEL)
        .setCharacteristic(Characteristic.SerialNumber, SERIAL_NUMBER);

    this.switchService = new Service.Switch(ACCESSORY_NAME); // seems don't need same as ACCESSORY_NAME
    this.switchService
        .getCharacteristic(Characteristic.On)
            .on('get', this.getSwitchOnCharacteristic.bind(this))
            .on('set', this.setSwitchOnCharacteristic.bind(this));

    return [ this.informationService, this.switchService ];
};


/*
 * handler for getting switch state
 */
CurtainSwitch.prototype.getSwitchOnCharacteristic = function (next) {
    var _this = this;

    this.curtainControl.getIsOn()
        .then(function (isOn) {
            _this.log("getSwitchOnCharacteristic state: " + (isOn ? "opened" : "closed"));
            next(null, isOn);
        })
        .catch(function (error) {
            _this.log("getSwitchOnCharacteristic error: " + JSON.stringify(error));
            next(new Error('getSwitchOnCharacteristic new ERROR'));
        });
};

/*
 * handler for setting switch state
 */
CurtainSwitch.prototype.setSwitchOnCharacteristic = function (targetState, next) {
    var _this = this;

    this.log("setSwitchOnCharacteristic START - targetState: " + (targetState ? "opened" : "closed"));

    (targetState === true ? this.curtainControl.open() : this.curtainControl.close())
        .then(function () {
            _this.log("setSwitchOnCharacteristic result: success");
            next();
        })
        .catch(function (error) {
            _this.log("setSwitchOnCharacteristic error: " + JSON.stringify(error));
            next(new Error('setSwitchOnCharacteristic new ERROR'));
        });
};
